-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2020 at 05:36 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xinemax`
--

-- --------------------------------------------------------

--
-- Table structure for table `bilik`
--

CREATE TABLE `bilik` (
  `NoBilik` int(5) NOT NULL,
  `BilanganTempatDuduk` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bilik`
--

INSERT INTO `bilik` (`NoBilik`, `BilanganTempatDuduk`) VALUES
(0, 50),
(1, 60);

-- --------------------------------------------------------

--
-- Table structure for table `pekerja`
--

CREATE TABLE `pekerja` (
  `idPekerja` varchar(30) NOT NULL,
  `kataLaluanPk` varchar(255) NOT NULL,
  `namaPekerja` varchar(255) NOT NULL,
  `telefonPekerja` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pekerja`
--

INSERT INTO `pekerja` (`idPekerja`, `kataLaluanPk`, `namaPekerja`, `telefonPekerja`) VALUES
('D1002', '12345678', 'Pekerja1', '016-12432234'),
('D3638', 'd3638', 'Lee Weng Hong', '018-9876651'),
('D4124', '213444', 'testcsv', '1230234'),
('D4134', '213444', 'testcsv2', '1230234'),
('D4311', '213444', 'testcsv2', '1230234'),
('D5415', '1234124123', 'Pekerja 3', '016-4215677'),
('D6771', '12345678', 'Pekerja2', '011-123412344'),
('E3211', 'testcase', 'main account', '016-5217456');

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE `pengurus` (
  `idPengurus` varchar(30) NOT NULL,
  `katalaluanPg` varchar(255) NOT NULL,
  `namaPengurus` varchar(50) NOT NULL,
  `telefonPengurus` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengurus`
--

INSERT INTO `pengurus` (`idPengurus`, `katalaluanPg`, `namaPengurus`, `telefonPengurus`) VALUES
('F7001', 'ytg6544', 'Choo Chen Zhung', '0165217456');

-- --------------------------------------------------------

--
-- Table structure for table `telefonpekerja`
--

CREATE TABLE `telefonpekerja` (
  `telefonPekerja` varchar(30) NOT NULL,
  `namaPekerja` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `telefonpekerja`
--

INSERT INTO `telefonpekerja` (`telefonPekerja`, `namaPekerja`) VALUES
('011-123412344', 'Pekerja2'),
('016-12432234', 'Pekerja1'),
('016-4215677', 'Pekerja 3'),
('018-9876651', 'Lee Weng Hong');

-- --------------------------------------------------------

--
-- Table structure for table `tiket`
--

CREATE TABLE `tiket` (
  `idTiket` varchar(30) NOT NULL,
  `idkedudukan` varchar(255) DEFAULT NULL,
  `tarikhJualan` date NOT NULL,
  `bilTiket` int(20) NOT NULL,
  `jumlahHarga` decimal(30,0) NOT NULL,
  `wayang` varchar(255) DEFAULT NULL,
  `masatayang` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tiket`
--

INSERT INTO `tiket` (`idTiket`, `idkedudukan`, `tarikhJualan`, `bilTiket`, `jumlahHarga`, `wayang`, `masatayang`) VALUES
('G117', 'A01 A02 A03', '2020-03-16', 3, '33', 'Sonic the Hedgehog', '1830'),
('G171', 'E02, E03', '2020-03-25', 2, '22', 'John Wick - Keanu Reeves', '0930'),
('G208', 'C03 C04 C05 C06', '2020-03-25', 4, '41', 'Ip man 4', '1530'),
('G217', 'A06 A07 B06 B07', '2020-04-05', 4, '36', 'Alita - Battle Angel', '1830'),
('G301', 'D01 D02', '2020-03-25', 2, '16', 'Jurassic World : Fallen Kingdom', '1830'),
('G302', 'A01 A02 A03', '2020-04-05', 3, '33', 'Hobbs & Shaw', '1830'),
('G410', 'F01 F02 F03', '2020-03-25', 3, '27', 'Alita - Battle Angel', '1830'),
('G456', 'A01 A02', '2020-04-04', 2, '22', 'Hobbs & Shaw', '1230'),
('G466', 'A01', '2020-04-05', 1, '11', 'Jumanji : Welcome to the Jungle', '1230'),
('G624', 'A05 A06', '2020-03-25', 2, '16', 'Avengers : Endgame', '1530'),
('G635', 'B03 B04 B05', '2020-03-25', 3, '24', 'Ip man 4', '1530'),
('G703', 'A07 A08 A09 A10', '2020-03-25', 4, '36', 'Jumanji : Welcome to the Jungle', '0930'),
('G759', 'A01 A02', '2020-04-04', 2, '22', 'Hobbs & Shaw', '1230'),
('G767', 'A02 B02 C02', '2020-03-19', 3, '24', 'Hobbs & Shaw', '1830'),
('G828', 'A01 A02', '2020-04-04', 2, '22', 'Hobbs & Shaw', '1230'),
('G894', 'A01 A02', '2020-04-04', 2, '22', 'Hobbs & Shaw', '1230'),
('G899', 'A01 A02 A03', '2020-03-28', 3, '33', 'Ip man 4', '1830'),
('G924', 'A01 A02', '2020-04-04', 2, '22', 'Hobbs & Shaw', '1230'),
('G943', 'A03 A04 A05 B04', '2020-04-04', 4, '44', 'Avengers : Endgame', '0930'),
('G978', 'A01 A02', '2020-04-04', 2, '22', 'Hobbs & Shaw', '1230');

-- --------------------------------------------------------

--
-- Table structure for table `wayang`
--

CREATE TABLE `wayang` (
  `idWayang` varchar(30) NOT NULL,
  `namaWayang` varchar(50) NOT NULL,
  `masaWayangan` time(2) NOT NULL,
  `NoBilik` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wayang`
--

INSERT INTO `wayang` (`idWayang`, `namaWayang`, `masaWayangan`, `NoBilik`) VALUES
('H001', 'Alita - Battle Angel', '02:02:00.00', 2),
('H002', 'Jumanji : Welcome to the Jungle', '01:59:00.00', 6),
('H003', 'Fast and Furious : Hobbs & Shaw', '02:16:00.00', 3),
('H004', 'Ip Man 4: The Finale', '01:43:00.00', 4),
('H005', 'Avengers : Endgame', '03:05:00.00', 1),
('H006', 'Jurassic World : Fallen Kingdom', '02:10:00.00', 7),
('H007', 'Sonic the Hedgehog', '01:40:00.00', 8),
('H008', 'John Wick - Keanu Reeves', '01:45:00.00', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bilik`
--
ALTER TABLE `bilik`
  ADD PRIMARY KEY (`NoBilik`);

--
-- Indexes for table `pekerja`
--
ALTER TABLE `pekerja`
  ADD PRIMARY KEY (`idPekerja`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`idPengurus`);

--
-- Indexes for table `telefonpekerja`
--
ALTER TABLE `telefonpekerja`
  ADD PRIMARY KEY (`telefonPekerja`);

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`idTiket`);

--
-- Indexes for table `wayang`
--
ALTER TABLE `wayang`
  ADD PRIMARY KEY (`idWayang`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
