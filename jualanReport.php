<?php
session_start();

if(!isset($_SESSION['username'])) {
    header("Location: login.php");
    die();
}
$connect = mysqli_connect("localhost", "root", "", "xinemax");
$output = '';
$bulan = $_SESSION['bulan'];
if(isset($_POST["export"]))
{

    if ($bulan == 0){
        $query = "SELECT wayang, SUM(jumlahHarga) AS 'Jumlah' FROM tiket GROUP BY wayang";
        $output .= "Jualan Bagi Setahun" ;
    }else{
        $query = "SELECT wayang, SUM(jumlahHarga) AS 'Jumlah' FROM tiket WHERE EXTRACT(MONTH FROM tarikhJualan) = $bulan GROUP BY wayang";
        $output .= "Jualan Bagi Bulan" . $_SESSION['bulan'];
    }

    $result = mysqli_query($connect, $query);
    if(mysqli_num_rows($result) > 0)
    {
        $output .= '
   <table class="table" bordered="1">  
                    <tr>  
                         <th>Wayang</th>  
                         <th>Jumlah Jualan</th>  
                         
                    </tr>
  ';
        while($row = mysqli_fetch_array($result))
        {
            $output .= '
    <tr>  
                         <td>'.$row["wayang"].'</td>  
                         <td>'.$row["Jumlah"].'</td>  
                    </tr>
   ';
        }
        $output .= '</table>';
        $output .= "\n";
        $output .= "Jumlah : ". $_SESSION['Jumlah'];
        header('Content-Type: application/xls');
        header('Content-Disposition: attachment; filename=Jualan.xls');
        echo $output;
    }
}
?>