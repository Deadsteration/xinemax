<?php
//export.php
if(!isset($_SESSION['username'])) {
    header("Location: login.php");
    die();
}

$connect = mysqli_connect("localhost", "root", "", "xinemax");

$output = '';

if(isset($_POST["export"]))
{
    $query = "SELECT * FROM pekerja";
    $result = mysqli_query($connect, $query);
    if(mysqli_num_rows($result) > 0)
    {
        $output .= '
   <table class="table" bordered="1">  
                    <tr>  
                         <th>Id Pekerja</th>  
                         <th>Nama Pekerja </th>  
                         <th>Kata Laluan</th>  
                         <th>Telefon Pekerja</th>
                    </tr>
  ';
        while($row = mysqli_fetch_array($result))
        {
            $output .= '
    <tr>  
                         <td>'.$row["idPekerja"].'</td>  
                         <td>'.$row["namaPekerja"].'</td>  
                         <td>'.$row["kataLaluanPk"].'</td>  
                         <td>'.$row["telefonPekerja"].'</td>  
                    </tr>
   ';
        }
        $output .= '</table>';
        header('Content-Type: application/xls');
        header('Content-Disposition: attachment; filename=SenaraiPekerja.xls');
        echo $output;
    }
}
?>